﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class BuildTower : MonoBehaviour
{
    public Transform[] buildPoint;
    public Button[] buildButtons;
    public GameObject[] selectorButton;
    public GameObject towerObj;
    public GameObject BlockObj;
    public int towerCost ;
    public int blockCost ;
    public CoinSystem coinSystem;
    private GameObject sm;
    void Start()
    {
        sm = GameObject.FindWithTag("Sound");
    }

    // Update is called once per frame
    void Update()
    {

    }
    

    public void BuildTowerObj(int buildP)
    {
        if (coinSystem.coin >= towerCost)
        {
            sm.GetComponent<SoundManager>().PlayTowerBuildUpgrade();
            coinSystem.coin -= towerCost;
            Instantiate(towerObj.gameObject, buildPoint[buildP].position, Quaternion.identity);
            //towerObj.transform.parent = GameObject.FindWithTag("Finish");
            buildButtons[buildP].gameObject.SetActive(false);
            selectorButton[buildP].SetActive(false);
            coinSystem.CoinTextUpdate();
            this.gameObject.SetActive(false);
            GetComponent<OnSelector>().OffSelector();
            
        }
        else
        {    
            print("********** No COIN !!! **********");
            sm.GetComponent<SoundManager>().PlayNoCoin();
        }
    }
    public void BuildBlockObj(int buildP)
    {
        if (coinSystem.coin >= blockCost)
        {
            sm.GetComponent<SoundManager>().PlayTowerBuildUpgrade();
            coinSystem.coin -= blockCost;
            Instantiate(BlockObj.gameObject, buildPoint[buildP].position, Quaternion.identity);
            //towerObj.transform.parent = GameObject.FindWithTag("Finish");
            buildButtons[buildP].gameObject.SetActive(false);
            selectorButton[buildP].SetActive(false);
            coinSystem.CoinTextUpdate();
            this.gameObject.SetActive(false); 
            
        }
        else
        {
            sm.GetComponent<SoundManager>().PlayNoCoin();
            print("********** No COIN !!! **********");
        }
    }

    public void OpenCloseBuildPanel()
    {
        if (this.gameObject.activeInHierarchy)
        {
            this.gameObject.SetActive(false);
            GetComponent<OnSelector>().OffSelector();
        }
        else
        {
            this.gameObject.SetActive(true);
        }
    }
}

    

