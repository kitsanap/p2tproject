﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public float speedBullet;
    public int bulletDamege;
    public GameObject bullet;
    private Rigidbody2D rb2D;
    void Start()
    { 
        bulletDamege = 10;
       rb2D = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        rb2D.velocity = new Vector2(0, speedBullet);
    }

    public void InstantiateBullet(GameObject outPoint)
    {
        Instantiate(bullet, outPoint.transform.position, Quaternion.identity);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Finish"))
        {
            Destroy(gameObject);
        }
        
    }
    
}
