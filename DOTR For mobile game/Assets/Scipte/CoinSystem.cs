﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class CoinSystem : MonoBehaviour
{
    public int coin;
    public static int score;
    public int killCount;
    public Text coinTxt;
    public Text scoreTxt;
   // public GameObject gameOver;
    void Start()
    {
        score = 0;
        CoinTextUpdate();
    }

    // Update is called once per frame
    void Update()
    {

    }
   public void AddCoin(int getCoin)
    {
        coin += getCoin;
        CoinTextUpdate();
    }
   public void AddScore(int getScore)
   {
       score += getScore;
       ScoreTextUpdate();
   }

   public void AddKillCount()
   {
       killCount++;
       print("KILLCOUNT ==== "+killCount);
   }

   public void ResetKillCount()
   {

       killCount = 0;
   }

   public void CoinTextUpdate()
    {
        coinTxt.text = (":" + coin);
    }
  public void ScoreTextUpdate()
  {
      scoreTxt.text = score.ToString();
  }
  public static int SendScore()
  {
      return score;
  }

}
