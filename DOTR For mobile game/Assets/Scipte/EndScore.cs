﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EndScore : MonoBehaviour
{
    public Text scoreEndText;
    public Text HighScoreText;
    
    // Start is called before the first frame update
    void Start()
    {
        HighScoreText.text = PlayerPrefs.GetInt("HighScore", 0).ToString();
        int scoreEnd = CoinSystem.SendScore();
        scoreEndText.text = scoreEnd.ToString();
        if (scoreEnd >= PlayerPrefs.GetInt("HighScore", 0))
        {
            PlayerPrefs.SetInt("HighScore", scoreEnd);
            HighScoreText.text = scoreEnd.ToString();

        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
