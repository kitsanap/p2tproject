﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.Advertisements;
using Random = System.Random;

public class Enemy : MonoBehaviour
{
    private Rigidbody2D rb2D;
    public int enemyHp;
    public float maxEnemyHp;
    public float speed; 
    public int attackDamage;
    private float hitTime;
    private GameObject coinSystem;
    public Image HpBar;

    private GameObject sm;
    // Start is called before the first frame update
    void Start()
    {
        sm = GameObject.FindWithTag("Sound");
        coinSystem = GameObject.FindWithTag("CoinSys");
        rb2D = GetComponent<Rigidbody2D>();
    }
    
    // Update is called once per frame
    void FixedUpdate()
    {
        rb2D.velocity = Vector2.down*speed;
        UpdateHp();
    }
    

    private void OnTriggerEnter2D(Collider2D other)
    {
        int dmg;
        if (other.gameObject.CompareTag("Bullet"))
        {
            sm.GetComponent<SoundManager>().PlayEnemyHitBullet();
            dmg = other.gameObject.GetComponent<Bullet>().bulletDamege;
            enemyHp -= dmg;
            Destroy(other.gameObject);
            
            // gameObject.GetComponent<SpriteRenderer>().color = Color.white;
            if (enemyHp <= 0)
            {
                print("Tower Killed");
                Die();
            }
        }
        if (other.gameObject.CompareTag("Finish"))
        {
            SceneManager.LoadScene("Game Over");
            
                if (Advertisement.IsReady("video"))
                {
                    print("Ready");
                    Advertisement.Show("video");
                }
            
        }

        if (other.gameObject.CompareTag("Bomb"))
        {
            Die();
        }
    }
    void Die()
    {
        sm.GetComponent<SoundManager>().PlayEnemyDie02();
        coinSystem.GetComponent<CoinSystem>().AddScore(100);
        coinSystem.GetComponent<CoinSystem>().AddCoin(20);
        coinSystem.GetComponent<CoinSystem>().AddKillCount();
        Destroy(this.gameObject);
        
    }

    public void UpdateHp()
    {
        
        HpBar.fillAmount = enemyHp/maxEnemyHp;
    }

    private void OnMouseDown()
    {
        sm.GetComponent<SoundManager>().PlayEnemyHitPlayer();
        gameObject.GetComponent<SpriteRenderer>().color = new Color32(250,0,0,150);
        enemyHp -= 5;
        
        if (enemyHp <= 0)
        {
            Die();
            print("Player Killed");
        }
        print("Player Attack : HP "+enemyHp);
    }

    private void OnMouseUp()
    {
        gameObject.GetComponent<SpriteRenderer>().color = Color.white;
    }
}
