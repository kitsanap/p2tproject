﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class EnemySpawnPoint : MonoBehaviour
{
    public Transform[] spawnPos;
    public GameObject[] enemyPrefab;
    public int killCount;
    public int waveCount;
    public int repeatRate;
    public int killRemaining;
    public int spawnCount;
    public int maxSpawnCount;
    public bool swapWave;
    public CoinSystem coinsystem;
    public GameObject map1;
    public GameObject map2;
    public GameObject map3;


    public Text waveText;
    public Text remainingText;

    private void Awake()
    {
        
    }

    void Start()
    {    
        waveCount = 1;
        repeatRate = 8;
        InvokeRepeating("SpawnEnemy", 1, repeatRate);
        maxSpawnCount = 10 ;
        map1.SetActive(true);
    }


    private void Update()
    {
        killCount = coinsystem.killCount;
        killRemaining = maxSpawnCount - killCount;
        TextUpdate();
        
        if (swapWave == true)
        {
            waveCount++;
            CancelInvoke("SpawnEnemy");
            switch (waveCount)
            {
                case 1 :
                    InvokeRepeating("SpawnEnemy", 1, repeatRate);
                    break;
                case 2 :
                    InvokeRepeating("SpawnEnemy", 1, repeatRate);
                    break;
                case 3 :
                    InvokeRepeating("SpawnEnemy", 1, repeatRate);
                    break;
                case 4 :
                    InvokeRepeating("SpawnEnemy", 1, repeatRate);
                    break;
                case 5 :
                    map2.SetActive(true);
                    map1.SetActive(false);
                    InvokeRepeating("SpawnEnemy", 1, repeatRate);
                    break;
                case 6 :
                    InvokeRepeating("SpawnEnemy", 1, repeatRate);
                    break;
                case 7 :
                    InvokeRepeating("SpawnEnemy", 1, repeatRate);
                    break;
                case 8 :
                    InvokeRepeating("SpawnEnemy", 1, repeatRate);
                    break;
                case 9 :
                    map3.SetActive(true);
                    map2.SetActive(false);
                    InvokeRepeating("SpawnEnemy", 1, repeatRate);
                    break;
                case 10 :
                    InvokeRepeating("SpawnEnemy", 1, repeatRate);
                    break;
                case 11 :
                    InvokeRepeating("SpawnEnemy", 1, repeatRate);
                    break;
                case 12 :
                    InvokeRepeating("SpawnEnemy", 1, repeatRate);
                    break;
                case 13 :
                    InvokeRepeating("SpawnEnemy", 1, repeatRate);
                    break;
                case 14 :
                    InvokeRepeating("SpawnEnemy", 1, repeatRate);
                    break;
                case 15 :
                    InvokeRepeating("SpawnEnemy", 1, repeatRate);
                    break;
                case 16 :
                    InvokeRepeating("SpawnEnemy", 1, repeatRate);
                    break;
                case 17 :
                    InvokeRepeating("SpawnEnemy", 1, repeatRate);
                    break;
                case 18 :
                    InvokeRepeating("SpawnEnemy", 1, repeatRate);
                    break;
                case 19 :
                    InvokeRepeating("SpawnEnemy", 1, repeatRate);
                    break;
                case 20 :
                    InvokeRepeating("SpawnEnemy", 1, repeatRate);
                    break;
            }
            swapWave = false;
           
            
        }
        
        if (spawnCount == maxSpawnCount )
        {
            CancelInvoke("SpawnEnemy");
        }

        if (killRemaining == 0)
        {
            spawnCount = 0;
            maxSpawnCount += 10;
            coinsystem.ResetKillCount();
            swapWave = true;
        }
        

    }

    // Update is called once per frame

    public void SpawnEnemy()
    {
        int e1e2;
        e1e2 = Random.Range(0,2);
        int e1e2e3;
        e1e2e3 = Random.Range(0,3);

        int posNum;
        posNum = Random.Range(0,spawnPos.Length);
        //Instantiate(enemyPrefab[i],spawnPos[posNum].position,Quaternion.identity);
        
        switch (waveCount)
        {
            case 1:
                repeatRate = 5;
                spawnCount++;
                Instantiate(enemyPrefab[0],spawnPos[posNum].position,Quaternion.identity);
                break;
            case 2:
                repeatRate = 5;
                spawnCount++;
                Instantiate(enemyPrefab[e1e2],spawnPos[posNum].position,Quaternion.identity);
                break;
            case 3:
                repeatRate = 5;
                spawnCount++;
                Instantiate(enemyPrefab[e1e2],spawnPos[posNum].position,Quaternion.identity);
                break;
            case 4:
                repeatRate = 4;
                spawnCount++;
                Instantiate(enemyPrefab[e1e2],spawnPos[posNum].position,Quaternion.identity);
                break;
            case 5:
                repeatRate = 4;
                spawnCount++;
                Instantiate(enemyPrefab[e1e2e3],spawnPos[posNum].position,Quaternion.identity);
                break;
            case 6:
                repeatRate = 3;
                spawnCount++;
                Instantiate(enemyPrefab[e1e2e3],spawnPos[posNum].position,Quaternion.identity);
                break;
            case 7:
                repeatRate = 3;
                spawnCount++;
                Instantiate(enemyPrefab[e1e2e3],spawnPos[posNum].position,Quaternion.identity);
                break;
            case 8:
                repeatRate = 2;
                spawnCount++;
                Instantiate(enemyPrefab[e1e2e3],spawnPos[posNum].position,Quaternion.identity);
                break;
            case 9:
                repeatRate = 2;
                spawnCount++;
                Instantiate(enemyPrefab[e1e2e3],spawnPos[posNum].position,Quaternion.identity);
                break;
            case 10:
                repeatRate = 1;
                spawnCount++;
                Instantiate(enemyPrefab[e1e2e3],spawnPos[posNum].position,Quaternion.identity);
                break;
            case 11:
                repeatRate = 1;
                spawnCount++;
                Instantiate(enemyPrefab[e1e2e3],spawnPos[posNum].position,Quaternion.identity);
                break;
            case 12:
                repeatRate = 1;
                spawnCount++;
                Instantiate(enemyPrefab[e1e2e3],spawnPos[posNum].position,Quaternion.identity);
                break;
            case 13:
                repeatRate = 1;
                spawnCount++;
                Instantiate(enemyPrefab[e1e2e3],spawnPos[posNum].position,Quaternion.identity);
                break;
            case 14:
                repeatRate = 1;
                spawnCount++;
                Instantiate(enemyPrefab[e1e2e3],spawnPos[posNum].position,Quaternion.identity);
                break;
            case 15:
                repeatRate = 1;
                spawnCount++;
                Instantiate(enemyPrefab[e1e2e3],spawnPos[posNum].position,Quaternion.identity);
                break;
            case 16:
                repeatRate = 1;
                spawnCount++;
                Instantiate(enemyPrefab[e1e2e3],spawnPos[posNum].position,Quaternion.identity);
                break;
            case 17:
                repeatRate = 1;
                spawnCount++;
                Instantiate(enemyPrefab[e1e2e3],spawnPos[posNum].position,Quaternion.identity);
                break;
            case 18:
                repeatRate = 1;
                spawnCount++;
                Instantiate(enemyPrefab[e1e2e3],spawnPos[posNum].position,Quaternion.identity);
                break;
            case 19:
                repeatRate = 1;
                spawnCount++;
                Instantiate(enemyPrefab[e1e2e3],spawnPos[posNum].position,Quaternion.identity);
                break;
            case 20:
                repeatRate = 1;
                spawnCount++;
                Instantiate(enemyPrefab[e1e2e3],spawnPos[posNum].position,Quaternion.identity);
                break;
        }
       // print(posNum);
       // print(i);
        

    }

    private void TextUpdate()
    {
        remainingText.text = "Enemy : " + killRemaining;
        waveText.text = "Wave : " + waveCount;

    }


}
