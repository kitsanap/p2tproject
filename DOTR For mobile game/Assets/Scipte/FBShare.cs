﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Facebook.Unity;

public class FBShare : MonoBehaviour
{
    public int highSocre;
    // Start is called before the first frame update
    void Awake()
    {
        FB.Init(SetInit);
    }

    void SetInit()
    {
        if (FB.IsLoggedIn)
        {
            Debug.Log("FB is logged in");
        }
        else
        {
            Debug.Log("FB is not logged in");
        }
    }
    // Update is called once per frame
    void Update()
    {
        
    }
    public void FacebookShare()
    {
        highSocre = PlayerPrefs.GetInt("HighScore", 0);
        print("SHARING");
        FB.ShareLink(
            contentTitle: "My Highscore : " + highSocre.ToString(),
            contentURL: new System.Uri("https://drive.google.com/file/d/19DEXrZA0MuZKn8c123yavDx0ICTNW4-k/view?usp=sharing"),
            contentDescription: "Come to join us",
            photoURL: new Uri("https://sv1.picz.in.th/images/2019/12/04/iwQYg2.png")
        );

    }
}
