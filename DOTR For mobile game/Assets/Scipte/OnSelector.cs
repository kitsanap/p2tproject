﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class OnSelector : MonoBehaviour
{

    public GameObject[] selector;
    public Button[] buildButton;
    // Start is called before the first frame update
    void Start()
    {
       
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Selection(int numselect)
    {
        OffSelector();
        // buildButton[numselect].gameObject.SetActive(false);
        selector[numselect].SetActive(true);
        
        
    }

    public void OffSelector()
    {
        
        selector[0].SetActive(false);
        selector[1].SetActive(false);
        selector[2].SetActive(false);
        selector[3].SetActive(false);
        selector[4].SetActive(false);
        selector[5].SetActive(false);
        selector[6].SetActive(false);
        selector[7].SetActive(false);
        selector[8].SetActive(false);
        selector[9].SetActive(false);
        selector[10].SetActive(false);
        selector[11].SetActive(false);
        selector[12].SetActive(false);
        selector[13].SetActive(false);
        selector[14].SetActive(false);
        selector[15].SetActive(false);
        selector[16].SetActive(false);
        selector[17].SetActive(false);
        selector[18].SetActive(false);
        selector[19].SetActive(false);
    }
}
