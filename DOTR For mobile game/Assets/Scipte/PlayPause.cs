﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayPause : MonoBehaviour
{
    public GameObject playPausePanel;
    // Start is called before the first frame update
    void Start()
    {
        Time.timeScale = 1;
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void PauseGame()
    {
        playPausePanel.SetActive(true);
        Time.timeScale = 0;
        
    }

    public void ResumeGame()
    {
        playPausePanel.SetActive(false);
        Time.timeScale = 1;
        
    }
}
