﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sandbag : MonoBehaviour
{
    // Start is called before the first frame update
    //public Bullet bullet;
    ///public GameObject bulletPoint;
    public int towerHp;
    public int Cost;
    public BuildTower buildTower;
    private GameObject sm;
    
    void Start()
    {
        sm = GameObject.FindWithTag("Sound");
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    

    void OnCollisionEnter2D(Collision2D other)
    {
        int attackDm;
        attackDm = other.gameObject.GetComponent<Enemy>().attackDamage;
        if (other.gameObject.CompareTag("Enemy"))
        {
            gameObject.GetComponent<SpriteRenderer>().color = new Color32(250,0,0,150);
            print("hit");
            towerHp -= attackDm;
            print(towerHp);
            if (towerHp <= 0)
            {
                DestroyTower();
            }
        }
        if (other.gameObject.CompareTag("EnemyBomber"))
        {
            sm.GetComponent<SoundManager>().PlayBomber();
            towerHp -= attackDm;
            Destroy(other.gameObject);
            DestroyTower();
        }
    }

    private void OnCollisionExit2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("Enemy"))
        {
            gameObject.GetComponent<SpriteRenderer>().color = Color.white;
        }
    }


    void DestroyTower()
    {
        sm.GetComponent<SoundManager>().PlayTowerDestroy();
        transform.position = Vector3.left*1000;
        // buildTower.buildButtons[buildP].gameObject.SetActive(true);
        Destroy(this.gameObject);
    }
}
