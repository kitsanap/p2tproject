﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class SkillSystem : MonoBehaviour
{
    public float energy;
    private float maxEnergy =60;
    public GameObject Skill;
    public Transform Spawn; 
    Image energyFill;
    public Button SkillButton;
    private GameObject SM;
    void Start()
    {
    
        SM = GameObject.FindWithTag("Sound");
        energyFill = SkillButton.GetComponent<Image>();
        
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        UpdateEnergyFilled();
        if (energy < maxEnergy)
        {
            energy += 0.01F;
        }
        else
        {
            SkillButton.interactable = true;
        }
    }


    public void UseSkillBomb()
    {
        if (energy >= 60)
        {
            energy -= 60;
            Instantiate(Skill, Spawn.position, Quaternion.identity);
            SkillButton.interactable = false;
        }
    }

    private void UpdateEnergyFilled()
    {
        energyFill.fillAmount = energy/maxEnergy;

    }
}
