﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    public AudioClip clickButton;
    public AudioClip enemyDie01;
    public AudioClip enemyDie02;
    public AudioClip enemyHitBullet;
    public AudioClip enemyHitPlayer;
    public AudioClip enemyHit;
    public AudioClip gameOver;
    public AudioClip menuClick;
    public AudioClip bombSound;
    public AudioClip towerShoot;
    public AudioClip towerBuildUpgrade;
    public AudioClip noCoin;
    public AudioClip towerDestroy;
    public AudioClip bomber;


    public void PlayClickButton()
    {
        GetComponent<AudioSource>().PlayOneShot(clickButton);
    }
    public void PlayEnemyDie01()
    {
        GetComponent<AudioSource>().PlayOneShot(enemyDie01);
    }
    public void PlayEnemyDie02()
    {
        GetComponent<AudioSource>().PlayOneShot(enemyDie02);
    }
    public void PlayEnemyHitBullet()
    {
        GetComponent<AudioSource>().PlayOneShot(enemyHitBullet);
    }
    public void PlayEnemyHitPlayer()
    {
        GetComponent<AudioSource>().PlayOneShot(enemyHitPlayer);
    }
    public void PlayEnemyHit()
    {
        GetComponent<AudioSource>().PlayOneShot(enemyHit);
    }
    public void PlayGameOver()
    {
        GetComponent<AudioSource>().PlayOneShot(gameOver);
    }
    public void PlayMenuClick()
    {
        GetComponent<AudioSource>().PlayOneShot(menuClick);
    }
    public void PlayBombSound()
    {
        GetComponent<AudioSource>().PlayOneShot(bombSound);
    }
    public void PlayTowerShoot()
    {
        GetComponent<AudioSource>().PlayOneShot(towerShoot);
    }
    public void PlayTowerBuildUpgrade()
    {
        GetComponent<AudioSource>().PlayOneShot(towerBuildUpgrade);
    }
    public void PlayTowerDestroy()
    {
        GetComponent<AudioSource>().PlayOneShot(towerDestroy);
    }
    public void PlayNoCoin()
    {
        GetComponent<AudioSource>().PlayOneShot(noCoin);
    }
    public void PlayBomber()
    {
        GetComponent<AudioSource>().PlayOneShot(bomber);
    }
    
}
