﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StartGame : MonoBehaviour
{
    
    
    void Start()
    {
        Time.timeScale = 1;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void GotoGameScene()
    {
        SceneManager.LoadScene("Gameplay");
    }

    public void GotoTutorial()
    {
        SceneManager.LoadScene("TutorialScenes");
    }

    public void GotoHighScore()
    {
        SceneManager.LoadScene("Highscore");

    }

    public void ExitGame()
    {
        Application.Quit();
    }


}
