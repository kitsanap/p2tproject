﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class Tower : MonoBehaviour
{
    // Start is called before the first frame update
    public Bullet bullet;
    public GameObject bulletPoint;
    public GameObject bulletPointLv2P1;
    public GameObject bulletPointLv2P2;
    public int towerHp;
    public int Cost;
    public BuildTower buildTower;
    public GameObject upgradeButton;
    public GameObject deletelButton;
    public GameObject coinSystem;
    //public Sprite lv2;
    private Animator animator;
    private GameObject sm;
    
    void Start()
    {
        sm = GameObject.FindWithTag("Sound");
        animator = GetComponent<Animator>();
        towerHp += 100;
        InvokeRepeating("BulletSpawn",1,3);
        coinSystem = GameObject.FindWithTag("CoinSys");
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void BulletSpawn()
    {
        sm.GetComponent<SoundManager>().PlayTowerShoot();
        bullet.InstantiateBullet(bulletPoint);
    }

    void BulletSpawnLV2P1()
    {
        bullet.InstantiateBullet(bulletPointLv2P1);
        
    }
    void BulletSpawnLV2P2()
    {
        bullet.InstantiateBullet(bulletPointLv2P2);
        
    }
    

    private void OnMouseDown()
    {
        sm.GetComponent<SoundManager>().PlayMenuClick();
        if (animator.GetInteger("LvTower") == 2)
        {
            upgradeButton.SetActive(false);
        }
        else if (upgradeButton.activeInHierarchy)
        {
            upgradeButton.SetActive(false);
        }
        else
        {
            upgradeButton.SetActive(true);
        }

        if (deletelButton.activeInHierarchy)
        {
            deletelButton.SetActive(false);
        }
        else
        {
            deletelButton.SetActive(true);
        }
    }

    public void UpGradeTower()
    {
        if (coinSystem.GetComponent<CoinSystem>().coin >= 300)
        {
            sm.GetComponent<SoundManager>().PlayTowerBuildUpgrade();
            print("Upgrade สำเร็จ");
            coinSystem.GetComponent<CoinSystem>().coin -= 300;
            coinSystem.GetComponent<CoinSystem>().CoinTextUpdate();
            upgradeButton.SetActive(false);
            deletelButton.SetActive(false);
            CancelInvoke("BulletSpawn");
            InvokeRepeating("BulletSpawnLV2P1", 2, 3);
            InvokeRepeating("BulletSpawnLV2P2", 1, 3);
            //this.gameObject.GetComponent<SpriteRenderer>().sprite = lv2;
            animator.SetInteger("LvTower",2);
            towerHp += 150;
        }
        else
        {
            sm.GetComponent<SoundManager>().PlayNoCoin();
            print("********** No COIN !!! **********");
        }

    }

    void OnCollisionEnter2D(Collision2D other)
    {
        int attackDm;
        attackDm = other.gameObject.GetComponent<Enemy>().attackDamage;
        if (other.gameObject.CompareTag("Enemy")) {
            sm.GetComponent<SoundManager>().PlayEnemyHit();
            gameObject.GetComponent<SpriteRenderer>().color = new Color32(250,0,0,150);
            print("hit");
            towerHp -= attackDm;
            print(towerHp);
            if (towerHp <= 0)
            {
                DestroyTower();
            }
        }

        if (other.gameObject.CompareTag("EnemyBomber"))
        {
            sm.GetComponent<SoundManager>().PlayBomber();
            towerHp -= attackDm;
            Destroy(other.gameObject);
            DestroyTower();
        }
    }

    private void OnCollisionExit2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("Enemy"))
        {
            gameObject.GetComponent<SpriteRenderer>().color = Color.white;
        }
    }


   public void DestroyTower()
    {
        sm.GetComponent<SoundManager>().PlayTowerDestroy();
        // buildTower.buildButtons[buildP].gameObject.SetActive(true);
        Destroy(this.gameObject);
    }

   public void RefundTower()
   {
       
       sm.GetComponent<SoundManager>().PlayTowerDestroy();
       coinSystem.GetComponent<CoinSystem>().coin += 50;
       Destroy(this.gameObject);
       
   }
}
