﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TutorialControl : MonoBehaviour
{
    public GameObject tutorial1;
    public GameObject tutorial2;
    public GameObject tutorial3;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    public void Show1()
    {
        print("1");
        tutorial1.SetActive(true);
        tutorial2.SetActive(false);
        
    }
    public void Show2()
    {
        print("2");
        tutorial1.SetActive(false);
        tutorial3.SetActive(false);
        tutorial2.SetActive(true);
        
    }
    
    public void Show3()
    {
        print("3");
        tutorial3.SetActive(true);
        tutorial2.SetActive(false);
    }

    public void GoToMenu()
    {
        SceneManager.LoadScene("Menu");
        
    }


}
